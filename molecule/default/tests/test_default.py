import os

import pytest

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


""" ansistrano.deploy: update code tests """


def test_drupal_webroot(host):
    f = host.file('/var/www/drupal/current/web')

    assert f.exists
    assert f.is_directory
    assert f.user == 'drupal'
    assert f.group == 'drupal'


def test_drupal_robots_file(host):
    c = host.run('curl http://127.0.0.1:80/robots.txt')

    assert c.rc == 0 and 'http://example.com/robots.txt' in c.stdout


""" ansistrano.deploy: after update code tests """


def test_composer_install(host):
    f = host.file('/var/www/drupal/current/vendor/bin/drush')

    assert f.exists


@pytest.mark.parametrize('path', [
  ('salt.txt'),
  ('web/sites/default/settings.local.php')
])
def test_shared_files(host, path):
    shared = '/var/www/drupal/shared/'
    drupal = '/var/www/drupal/current/'
    s = host.file('{}/{}'.format(shared, path))
    d = host.file('{}/{}'.format(drupal, path))

    assert s.exists
    assert s.content_string != ''
    assert s.user == 'drupal'
    assert s.user == 'drupal'
    assert d.exists
    assert d.content_string != ''
