---
# Prepares environment for role testing.

- name: Prepare role testing environment.
  hosts: all
  gather_facts: true

  vars:
    app_name: "drupal"
    app_web_dir: "/var/www/{{ app_name }}"
    app_docroot: "{{ app_web_dir }}/current/web"

    # ctorgalson.files vars.
    files_files:
      - path: "/var/www"
        state: directory
        mode: "u=rwx,go=rx"
      - path: "{{ app_web_dir }}"
        owner: "{{ app_name }}"
        group: "{{ app_name }}"
        state: directory
        mode: "u=rwx,go=rx"

    # geerlingguy.apache vars.
    apache_remove_default_vhost: true
    apache_vhosts:
      - servername: "{{ app_name }}"
        documentroot: "{{ app_docroot }}"

    # geerlingguy.mysql vars.
    mysql_packages:
      - "mariadb-client"
      - "mariadb-server"
      - "python-mysqldb"
    mysql_root_password: "{{ app_name }}_root_password"
    mysql_databases:
      - name: "{{ app_name }}_db"
    mysql_users:
      - name: "{{ app_name }}_user"
        password: "{{ app_name }}_password"
        host: "%"
        priv: "{{ app_name }}_db.*:ALL"

    # geerlingguy.php vars.
    php_default_version_debian: "7.3"
    php_packages:
      - "libapache2-mod-php{{ php_default_version_debian }}"
      - "libpcre3-dev"
      - "php{{ php_default_version_debian }}-common"
      - "php{{ php_default_version_debian }}-cli"
      - "php{{ php_default_version_debian }}-curl"
      - "php{{ php_default_version_debian }}-dev"
      - "php{{ php_default_version_debian }}-gd"
      - "php{{ php_default_version_debian }}-imap"
      - "php{{ php_default_version_debian }}-json"
      - "php{{ php_default_version_debian }}-mysql"
      - "php{{ php_default_version_debian }}-opcache"
      - "php{{ php_default_version_debian }}-xml"
      - "php{{ php_default_version_debian }}-mbstring"
      - "php-apcu"
      - "php-sqlite3"

    # weareinteractive.apt vars.
    apt_packages:
      - "curl"
      - "git"
      - "rsync"
      - "unzip"
      - "zip"
    apt_keys:
      # This key applies to both repositories below.
      - id: "14AA40EC0831756756D7F66C4F4EA0AAE5267A6C"
        keyserver: "keyserver.ubuntu.com"
    apt_repositories:
      - repo: "ppa:ondrej/apache2"
      - repo: "ppa:ondrej/php"

    # weareinteractive.users vars.
    users:
      - username: "{{ app_name }}"

  roles:
    - weareinteractive.users
    - weareinteractive.apt
    - ctorgalson.files
    - geerlingguy.apache
    - geerlingguy.php
    - geerlingguy.mysql

  tasks:
    - name: Install composer packages on task runner.
      composer:
        command: install
        working_dir: "{{ playbook_dir }}/../drupal"
      delegate_to: localhost

    - name: Copy db dump file to host.
      copy:
        src: "{{ playbook_dir }}/assets/demo_umami-fresh-20190306.sql"
        dest: "/tmp/demo_umami-fresh-20190306.sql"

    - name: Import database dump into newly-created db.
      mysql_db:
        name: "{{ app_name }}_db"
        state: import
        target: "/tmp/demo_umami-fresh-20190306.sql"

